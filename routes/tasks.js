const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const user = require('../models/user');
const task = require('../models/task');

const fetchUserID = require('../middlewares/tasksFetchUserID');
const auth = require('../middlewares/auth');

// Get all tasks along with users
router.get('/tasks', async (req, res) => {

    try {
        const tasks = await user.findAll({
            include: [{ model: task, attributes: ['description'] }],

            attributes: [['userName', 'User Name'], 'email']
        });

        if (tasks == null) {
            res.json({ message: "No tasks Available" })
        }

        res.json(tasks)
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Get all tasks for a user
router.get('/users/:userID/tasks', auth, async (req, res) => {

    try {

        if (req.params.userID == req.user.userID) {

            const tasks = await task.findAll(
                {
                    include: [{ model: user, attributes: [['userName', 'User Name'], 'email'] }],

                    attributes: {
                        exclude: ['createdAt', 'updatedAt']
                    },

                    where: {
                        userID: req.params.userID
                    }
                });

            if (tasks == null) {
                res.json({ message: "No tasks available for given user" })
            }
            else {
                res.json(tasks);
            }
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }

    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Add a new task for a user
router.post('/tasks', auth, async (req, res) => {

    try {

        if (req.body.userID == req.user.userID) {

            const tasks = await task.create(req.body);

            res.json({ message: "Task added successfully" })

        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }

    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Get a particular task for a user
router.get('/tasks/:taskID', auth, fetchUserID, async (req, res) => {

    try {
        if (req.fetch.userID == req.user.userID) {

            const tasks = await task.findByPk(req.params.taskID, {

                include: [{ model: user, attributes: [['userName', 'User Name'], 'email'] }],

                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'taskID']
                }
            })

            res.json(tasks)
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Update a particular task for a user
router.put('/tasks/:taskID', auth, fetchUserID, async (req, res) => {

    try {

        if (req.fetch.userID == req.user.userID) {

            await task.update(req.body, {
                where: {
                    taskID: req.params.taskID
                }
            })

            res.json({ message: "Task Updated Successfully" })
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Delete a particular task for a user
router.delete('/tasks/:taskID', auth, fetchUserID, async (req, res) => {

    try {
        if (req.fetch.userID == req.user.userID) {

            await task.destroy({
                where: {
                    taskID: req.params.taskID
                }
            })

            res.json({ message: "Task Deleted Successfully" })
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

module.exports = router