const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const user = require('../models/user')

const bcrypt = require('bcrypt');
const saltRounds = 10;

const jwt = require('jsonwebtoken')

router.post('/user/authenticate', async (req, res) => {

    ({ email, password } = req.body)

    const userReq = await user.findOne({

        where: {
            email: email
        },
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        }

    });

    if (userReq == null) {
        return res.status(404).json({ message: "Invalid email" })
    }

    const match = await bcrypt.compare(password, userReq.password);

    if (match) {

        userReq.password = undefined;
        //This is because we do not want to send password in jsontoken

        // Once we sign using below step, in 'result : userReq.dataValues'  we are passing all the data of the user
        // which is fetched from above using querry except password because that is made undefined
        // Instead of using jwt.sign({ result: userReq.dataValues } ,
        // it will be better to sign with above method i.e. jwt.sign({ ... userReq.dataValues }

        const jsonToken = jwt.sign({ ...userReq.dataValues }, process.env.secretKey, {
            expiresIn: "1h"
        });

        return res.json({
            message: "Login Successful",
            token: jsonToken
        });
    }
    else {
        res.json({ "message": "User ID or Password are Incorrect" })
    }
})

module.exports = router;