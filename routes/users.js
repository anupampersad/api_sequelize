const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const auth = require('../middlewares/auth');

const user = require('../models/user')

const bcrypt = require('bcrypt');
const saltRounds = 10;

// Gat all users
router.get('/users', async (req, res) => {

    try {
        const users = await user.findAll({
            attributes: {
                exclude: ['createdAt', 'updatedAt', 'userID', 'password']
            }
        });

        if(users == null){
            return res.json({message:"No users Registered"})
        }

        res.json(users)
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Get a particular user
router.get('/users/:userID', auth, async (req, res) => {

    try {
        if (req.params.userID == req.user.userID) {

            const users = await user.findByPk(req.params.userID, {
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                }
            });

            if (users == null) {
                return res.json({message:"User no longer exists"})
            }

            res.json(users)

        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }

})

// Add a new user
router.post('/users', async (req, res) => {

    try {
        const password = req.body.password;
        const encryptedPassword = await bcrypt.hash(password, saltRounds);

        const addUser = { ...req.body, password: encryptedPassword };

        await user.create(addUser);

        res.json({ message: "User Added Successfully" });
    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }

})

// Update a particular user
router.put('/users/:userID', auth, async (req, res) => {

    try {
        if (req.params.userID == req.user.userID) {

            ({ password } = req.body)

            const encryptedPassword = await bcrypt.hash(password, saltRounds);

            const updateUser = { ...req.body, password: encryptedPassword }

            await user.update(updateUser, {
                where: {
                    userID: req.params.userID
                }
            })

            res.json({ message: "User Updated Sucessfully" })
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }
    catch (error) {

        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Delete a user
router.delete('/users/:userID', auth, async (req, res) => {

    try {
        if (req.params.userID == req.user.userID) {

            await user.destroy({
                where: {
                    userID: req.params.userID
                }
            })
            res.json({ message: "User Deleted Successfully" })
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }

})

module.exports = router;