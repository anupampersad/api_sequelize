const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const user = require('../models/user');
const task = require('../models/task');
const subTask = require('../models/subTask');

const { fetchUserIDTaskID, fetchUserIDbody, fetchUserIDparams } = require('../middlewares/subTasksFetchUserID');
const auth = require('../middlewares/auth');


// Get all tasks along with subtasks for all user
router.get('/subTasks', async (req, res) => {

    try {
        const subTasks = await user.findAll({

            include: [{
                model: task,
                attributes: ['description'],
                include: [{
                    model: subTask,
                    attributes: ['description']
                }]
            }],

            attributes: [['userName', 'User Name'], 'email']
        });

        if (subTasks == null) {
            return res.json({ message: "No tasks or subtasks available" })
        }

        res.json(subTasks)

    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Add a new sub-task in a task for a user
router.post('/subTasks', auth, fetchUserIDbody, async (req, res) => {

    try {

        if (req.fetch.userID == req.user.userID) {

            await subTask.create(req.body);
            res.json({ message: "Sub Task added successfully" });

        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }
    catch {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }

})

// Get all tasks along with subtasks for a particular user
router.get('/users/:userID/subTasks', auth, async (req, res) => {

    try {
        if (req.params.userID == req.user.userID) {

            const subTasks = await user.findAll({

                include: [{
                    model: task,
                    attributes: ['description'],
                    include: [{
                        model: subTask,
                        attributes: ['description']
                    }]
                }],

                attributes: [['userName', 'User Name'], 'email'],

                where: {
                    userID: req.params.userID
                }
            });
            res.json(subTasks)
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Get all subtasks of a task of a user
router.get('/tasks/:taskID/subTasks', auth, fetchUserIDparams, async (req, res) => {

    try {
        if (req.fetch.userID == req.user.userID) {

            const subTasks = await task.findByPk(req.params.taskID, {

                include: [{
                    model: subTask,
                    attributes: ['description'],

                }],

                attributes: ['description']
            });

            res.json(subTasks)

        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Get a particular subTask of task for a user
router.get('/subTasks/:subTaskID', auth, fetchUserIDTaskID, async (req, res) => {

    try {

        if (req.fetch.userID == req.user.userID) {

            const subTasks = await subTask.findByPk(req.params.subTaskID, {

                attributes: [['description', 'Sub Task']],
                include: [{

                    model: task,
                    attributes: ['description']

                }]
            });
            if (subTasks == null) {
                return res.json({ message: "Invalid sub task id" })
            }

            res.json(subTasks)
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Update a subtask
router.put('/subTasks/:subTaskID', auth, fetchUserIDTaskID, async (req, res) => {

    try {
        if (req.fetch.userID == req.user.userID) {

            await subTask.update(req.body, {
                where: {
                    subTaskID: req.params.subTaskID,
                    taskID: req.fetch.taskID
                }
            })
            res.json({ message: "Sub task Updated Successfully" })
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Delete a sub task
router.delete('/subTasks/:subTaskID', auth, fetchUserIDTaskID, async (req, res) => {

    try {
        if (req.fetch.userID == req.user.userID) {

            await subTask.destroy({
                where: {
                    subTaskID: req.params.subTaskID,
                    taskID: req.fetch.taskID
                }
            })

            res.json({ message: "Sub task Deleted Successfully" })
        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

module.exports = router;