const { Sequelize, DataTypes } = require('sequelize');

const subTask = sequelize.define('subTask', {
    subTaskID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement:true,
        initialAutoIncrement:'1000'
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    taskID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        // references:{
        //     model:'tasks',
        //     key:'taskID'
        // }
    }
});

module.exports = subTask;