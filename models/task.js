const { Sequelize, DataTypes } = require('sequelize');

const task = sequelize.define('task', {
    taskID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        initialAutoIncrement: '100'
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    userID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        // references:{
        //     model:'users',
        //     key:'userID'
        // }
    }
});

module.exports = task;