const db = require('../database/connection');
const task = require('../models/task');

const tasksFetchUserID = async (req, res, next) => {

    try {
        const userIDReq = await task.findOne({

            where: {
                taskID: req.params.taskID
            },
            attributes: ['userID']
        });

        if (userIDReq == null) {
            res.status(404).json({ message: "Task id does not exist" })
        }
        else {
            req.fetch = userIDReq.dataValues;
            next()
        }
    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }

}

module.exports = tasksFetchUserID;