const db = require('../database/connection');
const task = require('../models/task');
const subTask = require('../models/subTask');


const fetchUserIDTaskID = async (req, res, next) => {

    try {

        const userIDTaskID = await subTask.findOne({

            include: [{ model: task, attributes: ['userID', 'taskID'] }],

            attributes: ['taskID'],

            where: {
                subTaskID: req.params.subTaskID
            }
        });

        if (userIDTaskID == null) {
            return res.status(404).json({ "message": "Sub task ID does not exist" })
        }

        else {
            req.fetch = userIDTaskID.task.dataValues;
            next()
        }

    }

    catch (error) {
        res.status(400).json({ "message": "Invalid Request" })
    }
}

const fetchUserIDbody = async (req, res, next) => {

    try {

        const userID = await subTask.findOne({

            include: [{ model: task, attributes: ['userID'] }],

            attributes: ['taskID'],

            where: {
                taskID: req.body.taskID
            }
        });

        if (userID == null) {

            const exists = await task.findByPk(req.params.taskID)

            if (exists) {
                return res.status(404).json({ "message": "No subtasks for given Task id" })
            }
            else {
                return res.status(404).json({ "message": "Task id does not exist" })
            }
        }

        else {
            req.fetch = userID.task.dataValues;
            next()
        }

    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }

}

const fetchUserIDparams = async (req, res, next) => {

    try {

        const userID = await subTask.findOne({

            include: [{ model: task, attributes: ['userID'] }],

            attributes: ['taskID'],

            where: {
                taskID: req.params.taskID
            }
        });

        if (userID == null) {

            const exists = await task.findByPk(req.params.taskID)

            if (exists) {
                return res.status(404).json({ "message": "No subtasks for given Task id" })
            }
            else {
                return res.status(404).json({ "message": "Task id does not exist" })
            }
        }

        else {
            req.fetch = userID.task.dataValues;
            next()
        }

    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }

}


module.exports = { fetchUserIDTaskID, fetchUserIDbody, fetchUserIDparams };