// Setting up express app
const express = require('express');
const app = express()
require('dotenv').config()

// Connecting to the database
const connectDB = require('./database/connection');
connectDB()

// Requiring Models
const user = require('./models/user')
const task = require('./models/task')
const subTask = require('./models/subTask')

app.use(express.json())

// Requiring routes
const userAuthenticate = require('./routes/authenticateUser')
const userRoutes = require('./routes/users')
const taskRoutes = require('./routes/tasks')
const subTaskRoutes = require('./routes/subTasks')

// // Associations
user.hasMany(task,{foreignKey: 'userID'}); // creates userID as foreign key in task model
task.hasMany(subTask,{foreignKey: 'taskID'}); // creates taskID as foreign key in task model

task.belongsTo(user,{foreignKey: 'userID'}) // creates userID as foreign key in task model
subTask.belongsTo(task,{foreignKey: 'taskID'}) // creates taskID as foreign key in task model

// Using Routes
app.use(userAuthenticate);
app.use(userRoutes);
app.use(taskRoutes);
app.use(subTaskRoutes);

app.listen(3000,()=>{
    console.log('Server running at port 3000')
})